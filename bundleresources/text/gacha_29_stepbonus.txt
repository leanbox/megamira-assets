＜ステップボーナスについて＞
・ステップボーナスが設定されているステップで「11回引く」を実行すると、キャラクター入手後に追加でアイテムを獲得できます。(アイテムの所持数が上限に達している場合はプレゼントボックスに送られます)
・5ステップ目から1ステップ目に戻ることで、ステップボーナスを再び獲得できるようになります。
・ステップボーナスが設定されているステップ数や内容については、「ステップボーナス詳細」のリストをご確認ください。
・★3キャラクターのカケラは「120個」入手することで、「ホーム＞ショップ＞カケラ交換所」にてキャラクターと交換できます。
