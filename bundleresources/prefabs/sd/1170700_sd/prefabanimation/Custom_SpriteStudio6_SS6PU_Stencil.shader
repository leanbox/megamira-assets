//////////////////////////////////////////
//
// NOTE: This is *not* a valid shader file
//
///////////////////////////////////////////
Shader "Custom/SpriteStudio6/SS6PU/Stencil" {
Properties {
_MainTex ("Base (RGB)", 2D) = "white" { }
_AlphaTex ("External Alpha", 2D) = "white" { }
_EnableExternalAlpha ("Enable External Alpha", Float) = 0
[Enum(UnityEngine.Rendering.StencilOp)] _StencilOperation ("Stencil Operation", Float) = 0
}
SubShader {
 Tags { "IGNOREPROJECTOR" = "true" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
 Pass {
  Tags { "IGNOREPROJECTOR" = "true" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
  ZWrite Off
  Cull Off
  GpuProgramID 32221
Program "vp" {
SubProgram "gles hw_tier00 " {
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
void main ()
{
  highp vec4 temp_1;
  highp vec4 tmpvar_2;
  temp_1.xy = _glesMultiTexCoord0.xy;
  temp_1.z = floor(_glesMultiTexCoord1.x);
  temp_1.w = 0.0;
  tmpvar_2 = temp_1;
  highp vec4 tmpvar_3;
  tmpvar_3.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_3.w = _glesMultiTexCoord1.y;
  highp vec4 tmpvar_4;
  highp vec4 tmpvar_5;
  tmpvar_5.w = 1.0;
  tmpvar_5.xyz = _glesVertex.xyz;
  tmpvar_4 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_5));
  temp_1 = tmpvar_4;
  gl_Position = tmpvar_4;
  xlv_COLOR0 = tmpvar_3;
  xlv_COLOR1 = _glesColor;
  xlv_TEXCOORD0 = tmpvar_2;
  xlv_TEXCOORD7 = tmpvar_4;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD0;
void main ()
{
  lowp vec4 tmpvar_1;
  tmpvar_1 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  if ((xlv_COLOR0.w >= tmpvar_1.w)) {
    discard;
  };
  gl_FragData[0] = vec4(0.0, 0.0, 0.0, 0.0);
}


#endif
"
}
SubProgram "gles hw_tier01 " {
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
void main ()
{
  highp vec4 temp_1;
  highp vec4 tmpvar_2;
  temp_1.xy = _glesMultiTexCoord0.xy;
  temp_1.z = floor(_glesMultiTexCoord1.x);
  temp_1.w = 0.0;
  tmpvar_2 = temp_1;
  highp vec4 tmpvar_3;
  tmpvar_3.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_3.w = _glesMultiTexCoord1.y;
  highp vec4 tmpvar_4;
  highp vec4 tmpvar_5;
  tmpvar_5.w = 1.0;
  tmpvar_5.xyz = _glesVertex.xyz;
  tmpvar_4 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_5));
  temp_1 = tmpvar_4;
  gl_Position = tmpvar_4;
  xlv_COLOR0 = tmpvar_3;
  xlv_COLOR1 = _glesColor;
  xlv_TEXCOORD0 = tmpvar_2;
  xlv_TEXCOORD7 = tmpvar_4;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD0;
void main ()
{
  lowp vec4 tmpvar_1;
  tmpvar_1 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  if ((xlv_COLOR0.w >= tmpvar_1.w)) {
    discard;
  };
  gl_FragData[0] = vec4(0.0, 0.0, 0.0, 0.0);
}


#endif
"
}
SubProgram "gles hw_tier02 " {
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
void main ()
{
  highp vec4 temp_1;
  highp vec4 tmpvar_2;
  temp_1.xy = _glesMultiTexCoord0.xy;
  temp_1.z = floor(_glesMultiTexCoord1.x);
  temp_1.w = 0.0;
  tmpvar_2 = temp_1;
  highp vec4 tmpvar_3;
  tmpvar_3.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_3.w = _glesMultiTexCoord1.y;
  highp vec4 tmpvar_4;
  highp vec4 tmpvar_5;
  tmpvar_5.w = 1.0;
  tmpvar_5.xyz = _glesVertex.xyz;
  tmpvar_4 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_5));
  temp_1 = tmpvar_4;
  gl_Position = tmpvar_4;
  xlv_COLOR0 = tmpvar_3;
  xlv_COLOR1 = _glesColor;
  xlv_TEXCOORD0 = tmpvar_2;
  xlv_TEXCOORD7 = tmpvar_4;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD0;
void main ()
{
  lowp vec4 tmpvar_1;
  tmpvar_1 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  if ((xlv_COLOR0.w >= tmpvar_1.w)) {
    discard;
  };
  gl_FragData[0] = vec4(0.0, 0.0, 0.0, 0.0);
}


#endif
"
}
SubProgram "gles3 hw_tier00 " {
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.z = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
float u_xlat0;
bool u_xlatb0;
void main()
{
    u_xlat0 = texture(_MainTex, vs_TEXCOORD0.xy).w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb0 = !!(vs_COLOR0.w>=u_xlat0);
#else
    u_xlatb0 = vs_COLOR0.w>=u_xlat0;
#endif
    if((int(u_xlatb0) * int(0xffffffffu))!=0){discard;}
    SV_Target0 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier01 " {
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.z = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
float u_xlat0;
bool u_xlatb0;
void main()
{
    u_xlat0 = texture(_MainTex, vs_TEXCOORD0.xy).w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb0 = !!(vs_COLOR0.w>=u_xlat0);
#else
    u_xlatb0 = vs_COLOR0.w>=u_xlat0;
#endif
    if((int(u_xlatb0) * int(0xffffffffu))!=0){discard;}
    SV_Target0 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier02 " {
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.z = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
float u_xlat0;
bool u_xlatb0;
void main()
{
    u_xlat0 = texture(_MainTex, vs_TEXCOORD0.xy).w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb0 = !!(vs_COLOR0.w>=u_xlat0);
#else
    u_xlatb0 = vs_COLOR0.w>=u_xlat0;
#endif
    if((int(u_xlatb0) * int(0xffffffffu))!=0){discard;}
    SV_Target0 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
"
}
SubProgram "gles hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
void main ()
{
  highp vec4 temp_1;
  highp vec4 tmpvar_2;
  temp_1.xy = _glesMultiTexCoord0.xy;
  temp_1.z = floor(_glesMultiTexCoord1.x);
  temp_1.w = 0.0;
  tmpvar_2 = temp_1;
  highp vec4 tmpvar_3;
  tmpvar_3.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_3.w = _glesMultiTexCoord1.y;
  highp vec4 tmpvar_4;
  highp vec4 tmpvar_5;
  tmpvar_5.w = 1.0;
  tmpvar_5.xyz = _glesVertex.xyz;
  tmpvar_4 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_5));
  temp_1 = tmpvar_4;
  gl_Position = tmpvar_4;
  xlv_COLOR0 = tmpvar_3;
  xlv_COLOR1 = _glesColor;
  xlv_TEXCOORD0 = tmpvar_2;
  xlv_TEXCOORD7 = tmpvar_4;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD0;
void main ()
{
  lowp vec4 pixel_1;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_1.xyz = tmpvar_2.xyz;
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_4;
  tmpvar_4 = mix (tmpvar_2.w, tmpvar_3.x, _EnableExternalAlpha);
  pixel_1.w = tmpvar_4;
  if ((xlv_COLOR0.w >= pixel_1.w)) {
    discard;
  };
  gl_FragData[0] = vec4(0.0, 0.0, 0.0, 0.0);
}


#endif
"
}
SubProgram "gles hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
void main ()
{
  highp vec4 temp_1;
  highp vec4 tmpvar_2;
  temp_1.xy = _glesMultiTexCoord0.xy;
  temp_1.z = floor(_glesMultiTexCoord1.x);
  temp_1.w = 0.0;
  tmpvar_2 = temp_1;
  highp vec4 tmpvar_3;
  tmpvar_3.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_3.w = _glesMultiTexCoord1.y;
  highp vec4 tmpvar_4;
  highp vec4 tmpvar_5;
  tmpvar_5.w = 1.0;
  tmpvar_5.xyz = _glesVertex.xyz;
  tmpvar_4 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_5));
  temp_1 = tmpvar_4;
  gl_Position = tmpvar_4;
  xlv_COLOR0 = tmpvar_3;
  xlv_COLOR1 = _glesColor;
  xlv_TEXCOORD0 = tmpvar_2;
  xlv_TEXCOORD7 = tmpvar_4;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD0;
void main ()
{
  lowp vec4 pixel_1;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_1.xyz = tmpvar_2.xyz;
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_4;
  tmpvar_4 = mix (tmpvar_2.w, tmpvar_3.x, _EnableExternalAlpha);
  pixel_1.w = tmpvar_4;
  if ((xlv_COLOR0.w >= pixel_1.w)) {
    discard;
  };
  gl_FragData[0] = vec4(0.0, 0.0, 0.0, 0.0);
}


#endif
"
}
SubProgram "gles hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
void main ()
{
  highp vec4 temp_1;
  highp vec4 tmpvar_2;
  temp_1.xy = _glesMultiTexCoord0.xy;
  temp_1.z = floor(_glesMultiTexCoord1.x);
  temp_1.w = 0.0;
  tmpvar_2 = temp_1;
  highp vec4 tmpvar_3;
  tmpvar_3.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_3.w = _glesMultiTexCoord1.y;
  highp vec4 tmpvar_4;
  highp vec4 tmpvar_5;
  tmpvar_5.w = 1.0;
  tmpvar_5.xyz = _glesVertex.xyz;
  tmpvar_4 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_5));
  temp_1 = tmpvar_4;
  gl_Position = tmpvar_4;
  xlv_COLOR0 = tmpvar_3;
  xlv_COLOR1 = _glesColor;
  xlv_TEXCOORD0 = tmpvar_2;
  xlv_TEXCOORD7 = tmpvar_4;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_TEXCOORD0;
void main ()
{
  lowp vec4 pixel_1;
  lowp vec4 tmpvar_2;
  tmpvar_2 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_1.xyz = tmpvar_2.xyz;
  lowp vec4 tmpvar_3;
  tmpvar_3 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_4;
  tmpvar_4 = mix (tmpvar_2.w, tmpvar_3.x, _EnableExternalAlpha);
  pixel_1.w = tmpvar_4;
  if ((xlv_COLOR0.w >= pixel_1.w)) {
    discard;
  };
  gl_FragData[0] = vec4(0.0, 0.0, 0.0, 0.0);
}


#endif
"
}
SubProgram "gles3 hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.z = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
float u_xlat0;
lowp float u_xlat10_0;
bool u_xlatb0;
mediump float u_xlat16_1;
lowp float u_xlat10_1;
void main()
{
    u_xlat10_0 = texture(_MainTex, vs_TEXCOORD0.xy).w;
    u_xlat10_1 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat16_1 = (-u_xlat10_0) + u_xlat10_1;
    u_xlat0 = _EnableExternalAlpha * u_xlat16_1 + u_xlat10_0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb0 = !!(vs_COLOR0.w>=u_xlat0);
#else
    u_xlatb0 = vs_COLOR0.w>=u_xlat0;
#endif
    if((int(u_xlatb0) * int(0xffffffffu))!=0){discard;}
    SV_Target0 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.z = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
float u_xlat0;
lowp float u_xlat10_0;
bool u_xlatb0;
mediump float u_xlat16_1;
lowp float u_xlat10_1;
void main()
{
    u_xlat10_0 = texture(_MainTex, vs_TEXCOORD0.xy).w;
    u_xlat10_1 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat16_1 = (-u_xlat10_0) + u_xlat10_1;
    u_xlat0 = _EnableExternalAlpha * u_xlat16_1 + u_xlat10_0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb0 = !!(vs_COLOR0.w>=u_xlat0);
#else
    u_xlatb0 = vs_COLOR0.w>=u_xlat0;
#endif
    if((int(u_xlatb0) * int(0xffffffffu))!=0){discard;}
    SV_Target0 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.z = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
float u_xlat0;
lowp float u_xlat10_0;
bool u_xlatb0;
mediump float u_xlat16_1;
lowp float u_xlat10_1;
void main()
{
    u_xlat10_0 = texture(_MainTex, vs_TEXCOORD0.xy).w;
    u_xlat10_1 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat16_1 = (-u_xlat10_0) + u_xlat10_1;
    u_xlat0 = _EnableExternalAlpha * u_xlat16_1 + u_xlat10_0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb0 = !!(vs_COLOR0.w>=u_xlat0);
#else
    u_xlatb0 = vs_COLOR0.w>=u_xlat0;
#endif
    if((int(u_xlatb0) * int(0xffffffffu))!=0){discard;}
    SV_Target0 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
"
}
}
Program "fp" {
SubProgram "gles hw_tier00 " {
""
}
SubProgram "gles hw_tier01 " {
""
}
SubProgram "gles hw_tier02 " {
""
}
SubProgram "gles3 hw_tier00 " {
""
}
SubProgram "gles3 hw_tier01 " {
""
}
SubProgram "gles3 hw_tier02 " {
""
}
SubProgram "gles hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
""
}
SubProgram "gles hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
""
}
SubProgram "gles hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
""
}
SubProgram "gles3 hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
""
}
SubProgram "gles3 hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
""
}
SubProgram "gles3 hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
""
}
}
}
}
}