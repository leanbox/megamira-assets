//////////////////////////////////////////
//
// NOTE: This is *not* a valid shader file
//
///////////////////////////////////////////
Shader "Custom/SpriteStudio6/SS6PU/Sprite" {
Properties {
_MainTex ("Base (RGB)", 2D) = "white" { }
_AlphaTex ("External Alpha", 2D) = "white" { }
_EnableExternalAlpha ("Enable External Alpha", Float) = 0
[Enum(UnityEngine.Rendering.BlendMode)] _BlendSource ("Blend Source", Float) = 0
[Enum(UnityEngine.Rendering.BlendMode)] _BlendDestination ("Blend Destination", Float) = 0
[Enum(UnityEngine.Rendering.BlendOp)] _BlendOperation ("Blend Operation", Float) = 0
[Enum(UnityEngine.Rendering.CompareFunction)] _CompareStencil ("Compare Stencil", Float) = 0
[Toggle] _ZWrite ("Write Z Buffer", Float) = 0
[Toggle(PS_NOT_DISCARD)] _NotDiscardPixel ("Not Discard Pixel", Float) = 0
[Toggle(PS_OUTPUT_PMA)] _OutputPixelPMA ("Output PreMultiplied Alpha", Float) = 0
}
SubShader {
 Tags { "IGNOREPROJECTOR" = "true" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
 Pass {
  Tags { "IGNOREPROJECTOR" = "true" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
  Blend Zero Zero, Zero Zero
  ZWrite Off
  Cull Off
  Stencil {
   Comp Disabled
   Pass Keep
   Fail Keep
   ZFail Keep
  }
  GpuProgramID 60606
Program "vp" {
SubProgram "gles hw_tier00 " {
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  pixel_5 = (texture2D (_MainTex, xlv_TEXCOORD0.xy) * xlv_COLOR0);
  if ((0.0 >= pixel_5.w)) {
    discard;
  };
  lowp float tmpvar_6;
  tmpvar_6 = pixel_5.w;
  pixelA_4 = tmpvar_6;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_7;
  tmpvar_7 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_7;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5.xyz = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w)).xyz;
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier01 " {
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  pixel_5 = (texture2D (_MainTex, xlv_TEXCOORD0.xy) * xlv_COLOR0);
  if ((0.0 >= pixel_5.w)) {
    discard;
  };
  lowp float tmpvar_6;
  tmpvar_6 = pixel_5.w;
  pixelA_4 = tmpvar_6;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_7;
  tmpvar_7 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_7;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5.xyz = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w)).xyz;
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier02 " {
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  pixel_5 = (texture2D (_MainTex, xlv_TEXCOORD0.xy) * xlv_COLOR0);
  if ((0.0 >= pixel_5.w)) {
    discard;
  };
  lowp float tmpvar_6;
  tmpvar_6 = pixel_5.w;
  pixelA_4 = tmpvar_6;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_7;
  tmpvar_7 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_7;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5.xyz = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w)).xyz;
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles3 hw_tier00 " {
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
lowp vec4 u_xlat10_0;
vec3 u_xlat1;
bool u_xlatb1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat13;
void main()
{
    u_xlat10_0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat0 = u_xlat10_0 * vs_COLOR0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0>=u_xlat0.w);
#else
    u_xlatb1 = 0.0>=u_xlat0.w;
#endif
    if((int(u_xlatb1) * int(0xffffffffu))!=0){discard;}
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat13 = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat13) + u_xlat16_3.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier01 " {
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
lowp vec4 u_xlat10_0;
vec3 u_xlat1;
bool u_xlatb1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat13;
void main()
{
    u_xlat10_0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat0 = u_xlat10_0 * vs_COLOR0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0>=u_xlat0.w);
#else
    u_xlatb1 = 0.0>=u_xlat0.w;
#endif
    if((int(u_xlatb1) * int(0xffffffffu))!=0){discard;}
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat13 = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat13) + u_xlat16_3.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier02 " {
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
lowp vec4 u_xlat10_0;
vec3 u_xlat1;
bool u_xlatb1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat13;
void main()
{
    u_xlat10_0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat0 = u_xlat10_0 * vs_COLOR0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0>=u_xlat0.w);
#else
    u_xlatb1 = 0.0>=u_xlat0.w;
#endif
    if((int(u_xlatb1) * int(0xffffffffu))!=0){discard;}
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat13 = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat13) + u_xlat16_3.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles hw_tier00 " {
Keywords { "PS_OUTPUT_PMA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  pixel_5 = (texture2D (_MainTex, xlv_TEXCOORD0.xy) * xlv_COLOR0);
  if ((0.0 >= pixel_5.w)) {
    discard;
  };
  lowp float tmpvar_6;
  tmpvar_6 = pixel_5.w;
  pixelA_4 = tmpvar_6;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_7;
  tmpvar_7 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_7;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5 = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w));
  pixel_5.xyz = (pixel_5.xyz * pixelA_4);
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier01 " {
Keywords { "PS_OUTPUT_PMA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  pixel_5 = (texture2D (_MainTex, xlv_TEXCOORD0.xy) * xlv_COLOR0);
  if ((0.0 >= pixel_5.w)) {
    discard;
  };
  lowp float tmpvar_6;
  tmpvar_6 = pixel_5.w;
  pixelA_4 = tmpvar_6;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_7;
  tmpvar_7 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_7;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5 = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w));
  pixel_5.xyz = (pixel_5.xyz * pixelA_4);
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier02 " {
Keywords { "PS_OUTPUT_PMA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  pixel_5 = (texture2D (_MainTex, xlv_TEXCOORD0.xy) * xlv_COLOR0);
  if ((0.0 >= pixel_5.w)) {
    discard;
  };
  lowp float tmpvar_6;
  tmpvar_6 = pixel_5.w;
  pixelA_4 = tmpvar_6;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_7;
  tmpvar_7 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_7;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5 = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w));
  pixel_5.xyz = (pixel_5.xyz * pixelA_4);
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles3 hw_tier00 " {
Keywords { "PS_OUTPUT_PMA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
lowp vec4 u_xlat10_0;
vec3 u_xlat1;
bool u_xlatb1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat13;
void main()
{
    u_xlat10_0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat0 = u_xlat10_0 * vs_COLOR0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0>=u_xlat0.w);
#else
    u_xlatb1 = 0.0>=u_xlat0.w;
#endif
    if((int(u_xlatb1) * int(0xffffffffu))!=0){discard;}
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat13 = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat1.xyz = u_xlat0.xyz * vec3(u_xlat13) + u_xlat16_3.xyz;
    u_xlat0.xyz = u_xlat0.www * u_xlat1.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier01 " {
Keywords { "PS_OUTPUT_PMA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
lowp vec4 u_xlat10_0;
vec3 u_xlat1;
bool u_xlatb1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat13;
void main()
{
    u_xlat10_0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat0 = u_xlat10_0 * vs_COLOR0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0>=u_xlat0.w);
#else
    u_xlatb1 = 0.0>=u_xlat0.w;
#endif
    if((int(u_xlatb1) * int(0xffffffffu))!=0){discard;}
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat13 = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat1.xyz = u_xlat0.xyz * vec3(u_xlat13) + u_xlat16_3.xyz;
    u_xlat0.xyz = u_xlat0.www * u_xlat1.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier02 " {
Keywords { "PS_OUTPUT_PMA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
lowp vec4 u_xlat10_0;
vec3 u_xlat1;
bool u_xlatb1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat13;
void main()
{
    u_xlat10_0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat0 = u_xlat10_0 * vs_COLOR0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0>=u_xlat0.w);
#else
    u_xlatb1 = 0.0>=u_xlat0.w;
#endif
    if((int(u_xlatb1) * int(0xffffffffu))!=0){discard;}
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat13 = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat1.xyz = u_xlat0.xyz * vec3(u_xlat13) + u_xlat16_3.xyz;
    u_xlat0.xyz = u_xlat0.www * u_xlat1.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles hw_tier00 " {
Keywords { "PS_NOT_DISCARD" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  pixel_5 = (texture2D (_MainTex, xlv_TEXCOORD0.xy) * xlv_COLOR0);
  lowp float tmpvar_6;
  tmpvar_6 = pixel_5.w;
  pixelA_4 = tmpvar_6;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_7;
  tmpvar_7 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_7;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5.xyz = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w)).xyz;
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier01 " {
Keywords { "PS_NOT_DISCARD" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  pixel_5 = (texture2D (_MainTex, xlv_TEXCOORD0.xy) * xlv_COLOR0);
  lowp float tmpvar_6;
  tmpvar_6 = pixel_5.w;
  pixelA_4 = tmpvar_6;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_7;
  tmpvar_7 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_7;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5.xyz = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w)).xyz;
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier02 " {
Keywords { "PS_NOT_DISCARD" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  pixel_5 = (texture2D (_MainTex, xlv_TEXCOORD0.xy) * xlv_COLOR0);
  lowp float tmpvar_6;
  tmpvar_6 = pixel_5.w;
  pixelA_4 = tmpvar_6;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_7;
  tmpvar_7 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_7;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5.xyz = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w)).xyz;
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles3 hw_tier00 " {
Keywords { "PS_NOT_DISCARD" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec4 u_xlat1;
lowp vec4 u_xlat10_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat12 = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat10_1 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat1 = u_xlat10_1 * vs_COLOR0;
    u_xlat2.xyz = u_xlat1.xyz * vs_TEXCOORD1.zzz + vec3(u_xlat12);
    u_xlat16_3.xyz = u_xlat0.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.x = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat1.xyz = u_xlat1.xyz * u_xlat0.xxx + u_xlat16_3.xyz;
    SV_Target0 = u_xlat1;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier01 " {
Keywords { "PS_NOT_DISCARD" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec4 u_xlat1;
lowp vec4 u_xlat10_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat12 = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat10_1 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat1 = u_xlat10_1 * vs_COLOR0;
    u_xlat2.xyz = u_xlat1.xyz * vs_TEXCOORD1.zzz + vec3(u_xlat12);
    u_xlat16_3.xyz = u_xlat0.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.x = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat1.xyz = u_xlat1.xyz * u_xlat0.xxx + u_xlat16_3.xyz;
    SV_Target0 = u_xlat1;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier02 " {
Keywords { "PS_NOT_DISCARD" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec4 u_xlat1;
lowp vec4 u_xlat10_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat12 = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat10_1 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat1 = u_xlat10_1 * vs_COLOR0;
    u_xlat2.xyz = u_xlat1.xyz * vs_TEXCOORD1.zzz + vec3(u_xlat12);
    u_xlat16_3.xyz = u_xlat0.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.x = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat1.xyz = u_xlat1.xyz * u_xlat0.xxx + u_xlat16_3.xyz;
    SV_Target0 = u_xlat1;
    return;
}

#endif
"
}
SubProgram "gles hw_tier00 " {
Keywords { "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  pixel_5 = (texture2D (_MainTex, xlv_TEXCOORD0.xy) * xlv_COLOR0);
  lowp float tmpvar_6;
  tmpvar_6 = pixel_5.w;
  pixelA_4 = tmpvar_6;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_7;
  tmpvar_7 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_7;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5 = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w));
  pixel_5.xyz = (pixel_5.xyz * pixelA_4);
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier01 " {
Keywords { "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  pixel_5 = (texture2D (_MainTex, xlv_TEXCOORD0.xy) * xlv_COLOR0);
  lowp float tmpvar_6;
  tmpvar_6 = pixel_5.w;
  pixelA_4 = tmpvar_6;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_7;
  tmpvar_7 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_7;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5 = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w));
  pixel_5.xyz = (pixel_5.xyz * pixelA_4);
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier02 " {
Keywords { "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  pixel_5 = (texture2D (_MainTex, xlv_TEXCOORD0.xy) * xlv_COLOR0);
  lowp float tmpvar_6;
  tmpvar_6 = pixel_5.w;
  pixelA_4 = tmpvar_6;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_7;
  tmpvar_7 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_7;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5 = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w));
  pixel_5.xyz = (pixel_5.xyz * pixelA_4);
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles3 hw_tier00 " {
Keywords { "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec3 u_xlat1;
vec4 u_xlat2;
lowp vec4 u_xlat10_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
void main()
{
    u_xlat0.x = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat4.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat10_2 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat2 = u_xlat10_2 * vs_COLOR0;
    u_xlat1.xyz = u_xlat2.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat16_3.xyz = u_xlat4.xyz * u_xlat1.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.xyz = u_xlat2.xyz * u_xlat0.xxx + u_xlat16_3.xyz;
    u_xlat2.xyz = u_xlat2.www * u_xlat0.xyz;
    SV_Target0 = u_xlat2;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier01 " {
Keywords { "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec3 u_xlat1;
vec4 u_xlat2;
lowp vec4 u_xlat10_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
void main()
{
    u_xlat0.x = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat4.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat10_2 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat2 = u_xlat10_2 * vs_COLOR0;
    u_xlat1.xyz = u_xlat2.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat16_3.xyz = u_xlat4.xyz * u_xlat1.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.xyz = u_xlat2.xyz * u_xlat0.xxx + u_xlat16_3.xyz;
    u_xlat2.xyz = u_xlat2.www * u_xlat0.xyz;
    SV_Target0 = u_xlat2;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier02 " {
Keywords { "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform lowp sampler2D _MainTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec3 u_xlat1;
vec4 u_xlat2;
lowp vec4 u_xlat10_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
void main()
{
    u_xlat0.x = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat4.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat10_2 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat2 = u_xlat10_2 * vs_COLOR0;
    u_xlat1.xyz = u_xlat2.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat16_3.xyz = u_xlat4.xyz * u_xlat1.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.xyz = u_xlat2.xyz * u_xlat0.xxx + u_xlat16_3.xyz;
    u_xlat2.xyz = u_xlat2.www * u_xlat0.xyz;
    SV_Target0 = u_xlat2;
    return;
}

#endif
"
}
SubProgram "gles hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  lowp vec4 tmpvar_6;
  tmpvar_6 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_5.xyz = tmpvar_6.xyz;
  lowp vec4 tmpvar_7;
  tmpvar_7 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_8;
  tmpvar_8 = mix (tmpvar_6.w, tmpvar_7.x, _EnableExternalAlpha);
  pixel_5.w = tmpvar_8;
  pixel_5 = (pixel_5 * xlv_COLOR0);
  if ((0.0 >= pixel_5.w)) {
    discard;
  };
  lowp float tmpvar_9;
  tmpvar_9 = pixel_5.w;
  pixelA_4 = tmpvar_9;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_10;
  tmpvar_10 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_10;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5.xyz = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w)).xyz;
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  lowp vec4 tmpvar_6;
  tmpvar_6 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_5.xyz = tmpvar_6.xyz;
  lowp vec4 tmpvar_7;
  tmpvar_7 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_8;
  tmpvar_8 = mix (tmpvar_6.w, tmpvar_7.x, _EnableExternalAlpha);
  pixel_5.w = tmpvar_8;
  pixel_5 = (pixel_5 * xlv_COLOR0);
  if ((0.0 >= pixel_5.w)) {
    discard;
  };
  lowp float tmpvar_9;
  tmpvar_9 = pixel_5.w;
  pixelA_4 = tmpvar_9;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_10;
  tmpvar_10 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_10;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5.xyz = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w)).xyz;
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  lowp vec4 tmpvar_6;
  tmpvar_6 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_5.xyz = tmpvar_6.xyz;
  lowp vec4 tmpvar_7;
  tmpvar_7 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_8;
  tmpvar_8 = mix (tmpvar_6.w, tmpvar_7.x, _EnableExternalAlpha);
  pixel_5.w = tmpvar_8;
  pixel_5 = (pixel_5 * xlv_COLOR0);
  if ((0.0 >= pixel_5.w)) {
    discard;
  };
  lowp float tmpvar_9;
  tmpvar_9 = pixel_5.w;
  pixelA_4 = tmpvar_9;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_10;
  tmpvar_10 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_10;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5.xyz = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w)).xyz;
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles3 hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
lowp float u_xlat10_1;
bool u_xlatb1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat13;
void main()
{
    u_xlat0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat10_1 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat1.x = (-u_xlat0.w) + u_xlat10_1;
    u_xlat0.w = _EnableExternalAlpha * u_xlat1.x + u_xlat0.w;
    u_xlat0 = u_xlat0 * vs_COLOR0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0>=u_xlat0.w);
#else
    u_xlatb1 = 0.0>=u_xlat0.w;
#endif
    if((int(u_xlatb1) * int(0xffffffffu))!=0){discard;}
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat13 = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat13) + u_xlat16_3.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
lowp float u_xlat10_1;
bool u_xlatb1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat13;
void main()
{
    u_xlat0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat10_1 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat1.x = (-u_xlat0.w) + u_xlat10_1;
    u_xlat0.w = _EnableExternalAlpha * u_xlat1.x + u_xlat0.w;
    u_xlat0 = u_xlat0 * vs_COLOR0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0>=u_xlat0.w);
#else
    u_xlatb1 = 0.0>=u_xlat0.w;
#endif
    if((int(u_xlatb1) * int(0xffffffffu))!=0){discard;}
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat13 = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat13) + u_xlat16_3.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
lowp float u_xlat10_1;
bool u_xlatb1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat13;
void main()
{
    u_xlat0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat10_1 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat1.x = (-u_xlat0.w) + u_xlat10_1;
    u_xlat0.w = _EnableExternalAlpha * u_xlat1.x + u_xlat0.w;
    u_xlat0 = u_xlat0 * vs_COLOR0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0>=u_xlat0.w);
#else
    u_xlatb1 = 0.0>=u_xlat0.w;
#endif
    if((int(u_xlatb1) * int(0xffffffffu))!=0){discard;}
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat13 = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat13) + u_xlat16_3.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_OUTPUT_PMA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  lowp vec4 tmpvar_6;
  tmpvar_6 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_5.xyz = tmpvar_6.xyz;
  lowp vec4 tmpvar_7;
  tmpvar_7 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_8;
  tmpvar_8 = mix (tmpvar_6.w, tmpvar_7.x, _EnableExternalAlpha);
  pixel_5.w = tmpvar_8;
  pixel_5 = (pixel_5 * xlv_COLOR0);
  if ((0.0 >= pixel_5.w)) {
    discard;
  };
  lowp float tmpvar_9;
  tmpvar_9 = pixel_5.w;
  pixelA_4 = tmpvar_9;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_10;
  tmpvar_10 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_10;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5 = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w));
  pixel_5.xyz = (pixel_5.xyz * pixelA_4);
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_OUTPUT_PMA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  lowp vec4 tmpvar_6;
  tmpvar_6 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_5.xyz = tmpvar_6.xyz;
  lowp vec4 tmpvar_7;
  tmpvar_7 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_8;
  tmpvar_8 = mix (tmpvar_6.w, tmpvar_7.x, _EnableExternalAlpha);
  pixel_5.w = tmpvar_8;
  pixel_5 = (pixel_5 * xlv_COLOR0);
  if ((0.0 >= pixel_5.w)) {
    discard;
  };
  lowp float tmpvar_9;
  tmpvar_9 = pixel_5.w;
  pixelA_4 = tmpvar_9;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_10;
  tmpvar_10 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_10;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5 = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w));
  pixel_5.xyz = (pixel_5.xyz * pixelA_4);
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_OUTPUT_PMA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  lowp vec4 tmpvar_6;
  tmpvar_6 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_5.xyz = tmpvar_6.xyz;
  lowp vec4 tmpvar_7;
  tmpvar_7 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_8;
  tmpvar_8 = mix (tmpvar_6.w, tmpvar_7.x, _EnableExternalAlpha);
  pixel_5.w = tmpvar_8;
  pixel_5 = (pixel_5 * xlv_COLOR0);
  if ((0.0 >= pixel_5.w)) {
    discard;
  };
  lowp float tmpvar_9;
  tmpvar_9 = pixel_5.w;
  pixelA_4 = tmpvar_9;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_10;
  tmpvar_10 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_10;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5 = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w));
  pixel_5.xyz = (pixel_5.xyz * pixelA_4);
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles3 hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_OUTPUT_PMA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
lowp float u_xlat10_1;
bool u_xlatb1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat13;
void main()
{
    u_xlat0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat10_1 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat1.x = (-u_xlat0.w) + u_xlat10_1;
    u_xlat0.w = _EnableExternalAlpha * u_xlat1.x + u_xlat0.w;
    u_xlat0 = u_xlat0 * vs_COLOR0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0>=u_xlat0.w);
#else
    u_xlatb1 = 0.0>=u_xlat0.w;
#endif
    if((int(u_xlatb1) * int(0xffffffffu))!=0){discard;}
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat13 = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat1.xyz = u_xlat0.xyz * vec3(u_xlat13) + u_xlat16_3.xyz;
    u_xlat0.xyz = u_xlat0.www * u_xlat1.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_OUTPUT_PMA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
lowp float u_xlat10_1;
bool u_xlatb1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat13;
void main()
{
    u_xlat0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat10_1 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat1.x = (-u_xlat0.w) + u_xlat10_1;
    u_xlat0.w = _EnableExternalAlpha * u_xlat1.x + u_xlat0.w;
    u_xlat0 = u_xlat0 * vs_COLOR0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0>=u_xlat0.w);
#else
    u_xlatb1 = 0.0>=u_xlat0.w;
#endif
    if((int(u_xlatb1) * int(0xffffffffu))!=0){discard;}
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat13 = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat1.xyz = u_xlat0.xyz * vec3(u_xlat13) + u_xlat16_3.xyz;
    u_xlat0.xyz = u_xlat0.www * u_xlat1.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_OUTPUT_PMA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
lowp float u_xlat10_1;
bool u_xlatb1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat13;
void main()
{
    u_xlat0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat10_1 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat1.x = (-u_xlat0.w) + u_xlat10_1;
    u_xlat0.w = _EnableExternalAlpha * u_xlat1.x + u_xlat0.w;
    u_xlat0 = u_xlat0 * vs_COLOR0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0>=u_xlat0.w);
#else
    u_xlatb1 = 0.0>=u_xlat0.w;
#endif
    if((int(u_xlatb1) * int(0xffffffffu))!=0){discard;}
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat13 = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat1.xyz = u_xlat0.xyz * vec3(u_xlat13) + u_xlat16_3.xyz;
    u_xlat0.xyz = u_xlat0.www * u_xlat1.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  lowp vec4 tmpvar_6;
  tmpvar_6 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_5.xyz = tmpvar_6.xyz;
  lowp vec4 tmpvar_7;
  tmpvar_7 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_8;
  tmpvar_8 = mix (tmpvar_6.w, tmpvar_7.x, _EnableExternalAlpha);
  pixel_5.w = tmpvar_8;
  pixel_5 = (pixel_5 * xlv_COLOR0);
  lowp float tmpvar_9;
  tmpvar_9 = pixel_5.w;
  pixelA_4 = tmpvar_9;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_10;
  tmpvar_10 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_10;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5.xyz = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w)).xyz;
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  lowp vec4 tmpvar_6;
  tmpvar_6 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_5.xyz = tmpvar_6.xyz;
  lowp vec4 tmpvar_7;
  tmpvar_7 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_8;
  tmpvar_8 = mix (tmpvar_6.w, tmpvar_7.x, _EnableExternalAlpha);
  pixel_5.w = tmpvar_8;
  pixel_5 = (pixel_5 * xlv_COLOR0);
  lowp float tmpvar_9;
  tmpvar_9 = pixel_5.w;
  pixelA_4 = tmpvar_9;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_10;
  tmpvar_10 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_10;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5.xyz = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w)).xyz;
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  lowp vec4 tmpvar_6;
  tmpvar_6 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_5.xyz = tmpvar_6.xyz;
  lowp vec4 tmpvar_7;
  tmpvar_7 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_8;
  tmpvar_8 = mix (tmpvar_6.w, tmpvar_7.x, _EnableExternalAlpha);
  pixel_5.w = tmpvar_8;
  pixel_5 = (pixel_5 * xlv_COLOR0);
  lowp float tmpvar_9;
  tmpvar_9 = pixel_5.w;
  pixelA_4 = tmpvar_9;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_10;
  tmpvar_10 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_10;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5.xyz = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w)).xyz;
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles3 hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
lowp float u_xlat10_0;
vec4 u_xlat1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
void main()
{
    u_xlat10_0 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat1 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat0.x = u_xlat10_0 + (-u_xlat1.w);
    u_xlat1.w = _EnableExternalAlpha * u_xlat0.x + u_xlat1.w;
    u_xlat0 = u_xlat1 * vs_COLOR0;
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat1.x = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xxx + u_xlat16_3.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
lowp float u_xlat10_0;
vec4 u_xlat1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
void main()
{
    u_xlat10_0 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat1 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat0.x = u_xlat10_0 + (-u_xlat1.w);
    u_xlat1.w = _EnableExternalAlpha * u_xlat0.x + u_xlat1.w;
    u_xlat0 = u_xlat1 * vs_COLOR0;
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat1.x = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xxx + u_xlat16_3.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
lowp float u_xlat10_0;
vec4 u_xlat1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
void main()
{
    u_xlat10_0 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat1 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat0.x = u_xlat10_0 + (-u_xlat1.w);
    u_xlat1.w = _EnableExternalAlpha * u_xlat0.x + u_xlat1.w;
    u_xlat0 = u_xlat1 * vs_COLOR0;
    u_xlat1.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat1.xyz = u_xlat0.xyz * vs_TEXCOORD1.zzz + u_xlat1.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat16_3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat1.x = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xxx + u_xlat16_3.xyz;
    SV_Target0 = u_xlat0;
    return;
}

#endif
"
}
SubProgram "gles hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  lowp vec4 tmpvar_6;
  tmpvar_6 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_5.xyz = tmpvar_6.xyz;
  lowp vec4 tmpvar_7;
  tmpvar_7 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_8;
  tmpvar_8 = mix (tmpvar_6.w, tmpvar_7.x, _EnableExternalAlpha);
  pixel_5.w = tmpvar_8;
  pixel_5 = (pixel_5 * xlv_COLOR0);
  lowp float tmpvar_9;
  tmpvar_9 = pixel_5.w;
  pixelA_4 = tmpvar_9;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_10;
  tmpvar_10 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_10;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5 = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w));
  pixel_5.xyz = (pixel_5.xyz * pixelA_4);
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  lowp vec4 tmpvar_6;
  tmpvar_6 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_5.xyz = tmpvar_6.xyz;
  lowp vec4 tmpvar_7;
  tmpvar_7 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_8;
  tmpvar_8 = mix (tmpvar_6.w, tmpvar_7.x, _EnableExternalAlpha);
  pixel_5.w = tmpvar_8;
  pixel_5 = (pixel_5 * xlv_COLOR0);
  lowp float tmpvar_9;
  tmpvar_9 = pixel_5.w;
  pixelA_4 = tmpvar_9;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_10;
  tmpvar_10 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_10;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5 = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w));
  pixel_5.xyz = (pixel_5.xyz * pixelA_4);
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
"#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec4 _glesColor;
attribute vec4 _glesMultiTexCoord0;
attribute vec4 _glesMultiTexCoord1;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_MatrixVP;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD7;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1 = _glesVertex;
  highp vec4 tmpvar_2;
  tmpvar_2 = _glesColor;
  highp vec4 temp_3;
  highp vec4 tmpvar_4;
  highp float tmpvar_5;
  tmpvar_5 = floor(_glesMultiTexCoord1.x);
  temp_3.xy = _glesMultiTexCoord0.xy;
  temp_3.z = tmpvar_5;
  temp_3.w = 0.0;
  tmpvar_4 = temp_3;
  highp vec4 tmpvar_6;
  tmpvar_6.xyz = vec3(1.0, 1.0, 1.0);
  tmpvar_6.w = _glesMultiTexCoord1.y;
  temp_3 = tmpvar_6;
  highp vec4 tmpvar_7;
  if ((2.0 > tmpvar_5)) {
    highp vec4 tmpvar_8;
    if ((1.0 > tmpvar_5)) {
      tmpvar_8 = vec4(1.0, 1.0, 0.0, 1.0);
    } else {
      tmpvar_8 = vec4(1.0, 0.0, 0.0, 1.0);
    };
    tmpvar_7 = tmpvar_8;
  } else {
    highp vec4 tmpvar_9;
    if ((3.0 > tmpvar_5)) {
      tmpvar_9 = vec4(1.0, 0.0, 0.0, -1.0);
    } else {
      tmpvar_9 = vec4(1.0, 1.0, 1.0, 1.0);
    };
    tmpvar_7 = tmpvar_9;
  };
  highp vec4 tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = tmpvar_1.xyz;
  tmpvar_10 = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_11));
  temp_3 = tmpvar_10;
  gl_Position = tmpvar_10;
  xlv_COLOR0 = tmpvar_6;
  xlv_COLOR1 = tmpvar_2;
  xlv_TEXCOORD0 = tmpvar_4;
  xlv_TEXCOORD7 = tmpvar_10;
  xlv_TEXCOORD1 = tmpvar_7;
}


#endif
#ifdef FRAGMENT
uniform sampler2D _MainTex;
uniform sampler2D _AlphaTex;
uniform highp float _EnableExternalAlpha;
varying highp vec4 xlv_COLOR0;
varying highp vec4 xlv_COLOR1;
varying highp vec4 xlv_TEXCOORD0;
varying highp vec4 xlv_TEXCOORD1;
void main ()
{
  lowp vec4 overlayParameter_1;
  highp float colorOverlayA_2;
  lowp vec4 colorOverlay_3;
  highp float pixelA_4;
  lowp vec4 pixel_5;
  lowp vec4 tmpvar_6;
  tmpvar_6 = texture2D (_MainTex, xlv_TEXCOORD0.xy);
  pixel_5.xyz = tmpvar_6.xyz;
  lowp vec4 tmpvar_7;
  tmpvar_7 = texture2D (_AlphaTex, xlv_TEXCOORD0.xy);
  highp float tmpvar_8;
  tmpvar_8 = mix (tmpvar_6.w, tmpvar_7.x, _EnableExternalAlpha);
  pixel_5.w = tmpvar_8;
  pixel_5 = (pixel_5 * xlv_COLOR0);
  lowp float tmpvar_9;
  tmpvar_9 = pixel_5.w;
  pixelA_4 = tmpvar_9;
  colorOverlay_3 = xlv_COLOR1;
  lowp float tmpvar_10;
  tmpvar_10 = colorOverlay_3.w;
  colorOverlayA_2 = tmpvar_10;
  overlayParameter_1 = xlv_TEXCOORD1;
  colorOverlay_3 = (colorOverlay_3 * colorOverlayA_2);
  pixel_5 = ((pixel_5 * (1.0 - 
    (colorOverlayA_2 * overlayParameter_1.y)
  )) + ((
    (vec4((1.0 - overlayParameter_1.z)) + (pixel_5 * overlayParameter_1.z))
   * colorOverlay_3) * overlayParameter_1.w));
  pixel_5.xyz = (pixel_5.xyz * pixelA_4);
  pixel_5.w = pixelA_4;
  gl_FragData[0] = pixel_5;
}


#endif
"
}
SubProgram "gles3 hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec4 u_xlat1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat4;
lowp float u_xlat10_4;
void main()
{
    u_xlat0.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat10_4 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat1 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat4 = u_xlat10_4 + (-u_xlat1.w);
    u_xlat1.w = _EnableExternalAlpha * u_xlat4 + u_xlat1.w;
    u_xlat1 = u_xlat1 * vs_COLOR0;
    u_xlat0.xyz = u_xlat1.xyz * vs_TEXCOORD1.zzz + u_xlat0.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat16_3.xyz = u_xlat0.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.x = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat0.xyz = u_xlat1.xyz * u_xlat0.xxx + u_xlat16_3.xyz;
    u_xlat1.xyz = u_xlat1.www * u_xlat0.xyz;
    SV_Target0 = u_xlat1;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec4 u_xlat1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat4;
lowp float u_xlat10_4;
void main()
{
    u_xlat0.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat10_4 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat1 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat4 = u_xlat10_4 + (-u_xlat1.w);
    u_xlat1.w = _EnableExternalAlpha * u_xlat4 + u_xlat1.w;
    u_xlat1 = u_xlat1 * vs_COLOR0;
    u_xlat0.xyz = u_xlat1.xyz * vs_TEXCOORD1.zzz + u_xlat0.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat16_3.xyz = u_xlat0.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.x = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat0.xyz = u_xlat1.xyz * u_xlat0.xxx + u_xlat16_3.xyz;
    u_xlat1.xyz = u_xlat1.www * u_xlat0.xyz;
    SV_Target0 = u_xlat1;
    return;
}

#endif
"
}
SubProgram "gles3 hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
"#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec4 vs_COLOR0;
out highp vec4 vs_COLOR1;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD7 = u_xlat0;
    vs_COLOR0.xyz = vec3(1.0, 1.0, 1.0);
    vs_COLOR0.w = in_TEXCOORD1.y;
    vs_COLOR1 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD0.w = 0.0;
    u_xlat0.x = floor(in_TEXCOORD1.x);
    vs_TEXCOORD0.z = u_xlat0.x;
    u_xlatb0.xyz = lessThan(u_xlat0.xxxx, vec4(2.0, 1.0, 3.0, 0.0)).xyz;
    u_xlat1 = (u_xlatb0.y) ? vec4(1.0, 1.0, 0.0, 1.0) : vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat2 = (u_xlatb0.z) ? vec4(1.0, 0.0, 0.0, -1.0) : vec4(1.0, 1.0, 1.0, 1.0);
    vs_TEXCOORD1 = (u_xlatb0.x) ? u_xlat1 : u_xlat2;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	float _EnableExternalAlpha;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2D _AlphaTex;
in highp vec4 vs_COLOR0;
in highp vec4 vs_COLOR1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec4 u_xlat1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
float u_xlat4;
lowp float u_xlat10_4;
void main()
{
    u_xlat0.x = (-vs_TEXCOORD1.z) + 1.0;
    u_xlat10_4 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat1 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat4 = u_xlat10_4 + (-u_xlat1.w);
    u_xlat1.w = _EnableExternalAlpha * u_xlat4 + u_xlat1.w;
    u_xlat1 = u_xlat1 * vs_COLOR0;
    u_xlat0.xyz = u_xlat1.xyz * vs_TEXCOORD1.zzz + u_xlat0.xxx;
    u_xlat2.xyz = vs_COLOR1.www * vs_COLOR1.xyz;
    u_xlat16_3.xyz = u_xlat0.xyz * u_xlat2.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vs_TEXCOORD1.www;
    u_xlat0.x = (-vs_COLOR1.w) * vs_TEXCOORD1.y + 1.0;
    u_xlat0.xyz = u_xlat1.xyz * u_xlat0.xxx + u_xlat16_3.xyz;
    u_xlat1.xyz = u_xlat1.www * u_xlat0.xyz;
    SV_Target0 = u_xlat1;
    return;
}

#endif
"
}
}
Program "fp" {
SubProgram "gles hw_tier00 " {
""
}
SubProgram "gles hw_tier01 " {
""
}
SubProgram "gles hw_tier02 " {
""
}
SubProgram "gles3 hw_tier00 " {
""
}
SubProgram "gles3 hw_tier01 " {
""
}
SubProgram "gles3 hw_tier02 " {
""
}
SubProgram "gles hw_tier00 " {
Keywords { "PS_OUTPUT_PMA" }
""
}
SubProgram "gles hw_tier01 " {
Keywords { "PS_OUTPUT_PMA" }
""
}
SubProgram "gles hw_tier02 " {
Keywords { "PS_OUTPUT_PMA" }
""
}
SubProgram "gles3 hw_tier00 " {
Keywords { "PS_OUTPUT_PMA" }
""
}
SubProgram "gles3 hw_tier01 " {
Keywords { "PS_OUTPUT_PMA" }
""
}
SubProgram "gles3 hw_tier02 " {
Keywords { "PS_OUTPUT_PMA" }
""
}
SubProgram "gles hw_tier00 " {
Keywords { "PS_NOT_DISCARD" }
""
}
SubProgram "gles hw_tier01 " {
Keywords { "PS_NOT_DISCARD" }
""
}
SubProgram "gles hw_tier02 " {
Keywords { "PS_NOT_DISCARD" }
""
}
SubProgram "gles3 hw_tier00 " {
Keywords { "PS_NOT_DISCARD" }
""
}
SubProgram "gles3 hw_tier01 " {
Keywords { "PS_NOT_DISCARD" }
""
}
SubProgram "gles3 hw_tier02 " {
Keywords { "PS_NOT_DISCARD" }
""
}
SubProgram "gles hw_tier00 " {
Keywords { "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles hw_tier01 " {
Keywords { "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles hw_tier02 " {
Keywords { "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles3 hw_tier00 " {
Keywords { "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles3 hw_tier01 " {
Keywords { "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles3 hw_tier02 " {
Keywords { "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
""
}
SubProgram "gles hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
""
}
SubProgram "gles hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
""
}
SubProgram "gles3 hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
""
}
SubProgram "gles3 hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
""
}
SubProgram "gles3 hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" }
""
}
SubProgram "gles hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles3 hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles3 hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles3 hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" }
""
}
SubProgram "gles hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" }
""
}
SubProgram "gles hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" }
""
}
SubProgram "gles3 hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" }
""
}
SubProgram "gles3 hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" }
""
}
SubProgram "gles3 hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" }
""
}
SubProgram "gles hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles3 hw_tier00 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles3 hw_tier01 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
""
}
SubProgram "gles3 hw_tier02 " {
Keywords { "ETC1_EXTERNAL_ALPHA" "PS_NOT_DISCARD" "PS_OUTPUT_PMA" }
""
}
}
}
}
}